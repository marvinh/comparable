package comparable;

public interface Comparable<E> {
    boolean isSmallerThan(E object);
    boolean isEqual(E object);
    boolean isGreaterThan(E object);

    default boolean isGreaterThanOrEqual(E object){
        return isGreaterThan(object) || isEqual(object);
    }

    default boolean isSmallerThanOrEqual(E object){
        return isSmallerThan(object) || isEqual(object);
    }

    default boolean isNotEqual(E object){
        return !isEqual(object);
    }
}
